package com.feebit;

/**
 * Created by 913573 on 2/20/2015.
 */

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration
@ComponentScan("com.feebit")
public class App {

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(App.class, args);
    }
}
