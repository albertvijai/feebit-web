package com.feebit.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 913573 on 2/20/2015.
 */
@Controller
public class HomeController {

    @RequestMapping("/home")
    public String home(HttpServletRequest request) {
        return "landing";
    }

    @RequestMapping("/")
    public String root(HttpServletRequest request) {
        return "login";
    }

    @RequestMapping("/login")
    public String login(HttpServletRequest request) {
        return "login";
    }

    @RequestMapping("/register")
    public String register(HttpServletRequest request) {
        return "register";
    }

    @RequestMapping("/landing")
    public String landing(HttpServletRequest request) {
        return "landing";
    }

}
